const CpfCnpj = require('../src');
const faker = require('faker-br');

describe('CNPJ', () => {

    const cnpj = faker.br.cnpj();

    it('should have numerals', () => {
        expect( CpfCnpj.onlyNumber( cnpj ) ).toMatch(/\d/);
    });

    it('should have 14 digits', () => {
        expect( CpfCnpj.lengthCompare(cnpj, 14)).toBe(true);
    });

    it('should not sequence unique digit', () => {
        expect( CpfCnpj.notSequenceDigit(cnpj) ).toBe(true)
    });

    it('should valid CNPJ', () => {
       expect( CpfCnpj.validCnpj(cnpj)).toBe(true);
    });

    it('should masquered CNPJ', () => {
       let cnpj = '12345678999900';
       expect( CpfCnpj.addMask(cnpj) ).toBe('12.345.678/9999-00');
    });

});

describe('CPF', () => {

    const cpf = faker.br.cpf();

    it('should have numerals', () => {
        expect( CpfCnpj.onlyNumber( cpf ) ).toMatch(/\d/);
    });

    it('should have 11 digits', () => {
        expect( CpfCnpj.lengthCompare(cpf, 11)).toBe(true);
    });

    it('should not repeat unique digit', () => {
        expect( CpfCnpj.notSequenceDigit(cpf) ).toBe(true)
    });

    it('should valid CPF', () => {
       expect( CpfCnpj.validCpf(cpf)).toBe(true);
    });

    it('should added mask in CPF number', () => {
        let cpf = '01234567890';
        expect( CpfCnpj.addMask( cpf ) ).toBe('012.345.678-90');
    });


});

// 28517714000190
