# CPF / CNPJ Validator

Permite validar CPF e CNPJ

## Instalação

Via npm:

    npm install cpf-cnpj-validator
    
## Uso

    const CpfCnpj = require "cpf-cnpj-validator"

    let isValidCpf = CpfCnpj.validCpf('012.345.678-90');
    let cpf = CpfCnpj.addMask('01234567890');
    cpf = CpfCnpj.onlyNumbers( cpf );
    
    let isValidCnpj = CpfCnpj.validCnpj('01.234.567/0001-89');
    let cnpj = CpfCnpj.addMask('01234567000189');
    cnpj = CpfCnpj.onlyNumbers( cnpj );

## Rodando testes

    jest
    
## Autor
    Edmar Baladeli <edmar@wtit.com.br> edmar@wtit.com.br
    
## Licença

 Este projeto está licenciado sob a licença MIT - consulte [MIT](https://choosealicense.com/licenses/mit/) para obter detalhes

    
