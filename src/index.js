class CpfCnpjValidator {

    validCnpj( cnpj ) {

        cnpj = this.onlyNumber( cnpj );

        if( !this.lengthCompare( cnpj, 14)  || !this.notSequenceDigit( cnpj ) ) {
            return false;
        }

        let digit   = 0;
        let sum     = 0;
        let number  = cnpj.substr(0,12);
        let verify  = cnpj.substr(cnpj.length-2,2);
        let factor  = "543298765432";
        let control = "";

        for( let j = 0; j < 2; j++ ) {
            sum = 0;

            for( let i = 0; i < 12; i++ ) {
                sum += number.substr(i,1) * factor.substr(i,1);
            }

            if( j === 1 ) {
                sum += digit * 2;
            }

            digit = (sum * 10) % 11;

            if( digit === 10 ) {
                digit = 0;
            }

            control += digit;
            factor   = "654329876543";
        }

        return ( verify === control );
    }

    validCpf(cpf) {

        cpf = this.onlyNumber(cpf);

        if( !this.lengthCompare( cpf, 11 ) ||  !this.notSequenceDigit( cpf ) ) {
            return false;
        }

        let number  = this.onlyNumber( cpf );
        let verify  = cpf.substr(cpf.length-2,2);
        let control = "";
        let start   = 2;
        let end     = 10;
        let digit   = 0;
        let sum     = 0;

        for( let i = 1; i <= 2; i++ ) {
            sum = 0;

            for( let j = start; j <= end; j++) {
                sum += number.substr((j-i-1),1)*(end+1+i-j);
            }

            if( i === 2 ) {
                sum += digit * 2;
            }

            digit = (sum * 10) % 11;

            if( digit === 10 ) {
                digit = 0;
            }

            control += digit;
            start    = 3;
            end      = 11;
        }

        return  ( control === verify );
    }

    onlyNumber( param ) {
        return param.replace(/\D/g, '' );
    }

    lengthCompare( param, compare ){
        return param.length === compare;
    }

    notSequenceDigit( param ) {
        let regex =  `/^[${param.substr(0,1)}]{1,}$/`;
        return !param.match( regex );
    }

    addMask( param ) {

        if( param.length === 14 ) {
            param = this._addMask( param, '##.###.###/####-##' );
        } else if ( param.length === 11 ) {
            param = this._addMask( param, '###.###.###-##');
        }

        return param;
    }

    _addMask( param, mask, masquerade = '#') {
        let last = '';
        let tmp  = '';
        let i    = 0;

        for( let x in mask) {
            if( mask[x] !== last && last !== '' && mask[x] !== masquerade ) {
                tmp = tmp + mask[x];
                continue;
            }

            if( param[i] ) {
                tmp = tmp + param[i];
            }

            i++;
            last = mask[x];
        }
        return tmp;
    }
}

module.exports = new CpfCnpjValidator();
